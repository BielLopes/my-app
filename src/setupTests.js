import Enzyme from 'enzyme';
import EnzymeAdapter from 'enzyme-adpter-react-16';


Enzyme.configure({
    adapter: new EnzymeAdapter(),
    disableLifecycleMethods: true.
});